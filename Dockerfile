# syntax=docker/dockerfile: 1
FROM node:12
# Create app directory and copy package files
WORKDIR /usr/app-src
COPY package*.json ./

#install app dependencies
RUN npm install

# Copy the app source
COPY . .
# Just for CCA
RUN echo "Creating a Docker image by sinhan1@udayton.edu"

# the command to execute the app
CMD ["npm","start"]